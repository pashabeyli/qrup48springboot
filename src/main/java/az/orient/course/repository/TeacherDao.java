package az.orient.course.repository;

import az.orient.course.model.Teacher;
import org.springframework.data.repository.CrudRepository;

public interface TeacherDao extends CrudRepository<Teacher,Long> {
}
