package az.orient.course;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class Qrup48springbootApplication  extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Qrup48springbootApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Qrup48springbootApplication.class);
    }

}
