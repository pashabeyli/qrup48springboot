package az.orient.course.service;

import az.orient.course.response.RespPaymentList;

public interface PaymentService {


    RespPaymentList getPaymentList();
}
